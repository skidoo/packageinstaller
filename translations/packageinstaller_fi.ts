<?xml version="1.0" ?><!DOCTYPE TS><TS language="fi" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="mainwindow.cpp" line="1182"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <location filename="mainwindow.cpp" line="1197"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Package Installer</source>
        <translation>Pakettien asennus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="42"/>
        <location filename="ui_mainwindow.h" line="440"/>
        <source>Popular Applications</source>
        <translation>Suositut sovellukset</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Package</source>
        <translation>Paketti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="436"/>
        <source>Info</source>
        <translation>Tiedot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <location filename="mainwindow.ui" line="363"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="435"/>
        <location filename="ui_mainwindow.h" line="462"/>
        <source>Description</source>
        <translation>Kuvaus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <location filename="mainwindow.ui" line="332"/>
        <location filename="ui_mainwindow.h" line="438"/>
        <location filename="ui_mainwindow.h" line="459"/>
        <source>search</source>
        <translation>hae</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Greyed out items have already been installed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Hallitse suosittuja paketteja&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Harmaannetut kohteet on jo asennettu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <location filename="ui_mainwindow.h" line="474"/>
        <source>Full App Catalog</source>
        <translation>Sovellusvalikoima kokonaisuudessaan</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="144"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Select source</source>
        <translation>Valitse lähde</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <location filename="mainwindow.cpp" line="1283"/>
        <location filename="ui_mainwindow.h" line="442"/>
        <source>Stable Repo</source>
        <translation>Vakaa (Stable) pakettivarasto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="ui_mainwindow.h" line="443"/>
        <source>MX Test Repo</source>
        <translation>MX Testiohjelmalähde</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="170"/>
        <location filename="mainwindow.cpp" line="1281"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Debian Backports Repo</source>
        <translation>Debian:in Takaporttiohjelmistolähde</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="454"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Suodata paketit niiden tilan suhteen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="mainwindow.ui" line="265"/>
        <location filename="mainwindow.cpp" line="1323"/>
        <location filename="ui_mainwindow.h" line="448"/>
        <location filename="ui_mainwindow.h" line="456"/>
        <source>All packages</source>
        <translation>Kaikki paketit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <location filename="mainwindow.cpp" line="1336"/>
        <location filename="ui_mainwindow.h" line="449"/>
        <source>Installed</source>
        <translation>Asennettu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="275"/>
        <location filename="mainwindow.cpp" line="1334"/>
        <location filename="ui_mainwindow.h" line="450"/>
        <source>Upgradable</source>
        <translation>Päivitettävissä</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="ui_mainwindow.h" line="451"/>
        <source>Not installed</source>
        <translation>Ei asennettu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="457"/>
        <source>Greyed out items have already been installed.</source>
        <translation>Harmaana olevat on jo asennettu.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="458"/>
        <source>Refresh list</source>
        <translation>Päivitä lista</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="464"/>
        <source>Package Name</source>
        <translation>Paketin nimi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="463"/>
        <source>Version</source>
        <translation>Versio</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="461"/>
        <source>Displayed</source>
        <translation>Näytetty</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <location filename="ui_mainwindow.h" line="465"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= Upgradable package. Newer version available in selected repository.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= Päivitettävä paketti. Uudempi versio on saatavilla valitussa ohjelmavarastossa.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="466"/>
        <source>Total packages:</source>
        <translation>Pakettien kokonaismäärä:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <location filename="ui_mainwindow.h" line="468"/>
        <source>Installed:</source>
        <translation>Asennettu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="463"/>
        <location filename="ui_mainwindow.h" line="470"/>
        <source>Upgradable:</source>
        <translation>Päivitettävissä:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="477"/>
        <location filename="ui_mainwindow.h" line="472"/>
        <source>Upgrade All</source>
        <translation>Päivitä kaikki</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="473"/>
        <source>Hide library and developer packages</source>
        <translation>Piilota kirjasto- ja kehityspaketit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="545"/>
        <location filename="ui_mainwindow.h" line="475"/>
        <source>Uninstall</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="579"/>
        <location filename="ui_mainwindow.h" line="477"/>
        <source>About this application</source>
        <translation>Tietoja tästä sovelluksesta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="582"/>
        <location filename="ui_mainwindow.h" line="479"/>
        <source>About...</source>
        <translation>Tietoja...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <location filename="ui_mainwindow.h" line="480"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="606"/>
        <location filename="ui_mainwindow.h" line="482"/>
        <source>Display help </source>
        <translation>Näytä ohje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="ui_mainwindow.h" line="484"/>
        <source>Help</source>
        <translation>Ohje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="615"/>
        <location filename="ui_mainwindow.h" line="485"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="628"/>
        <location filename="mainwindow.cpp" line="1230"/>
        <location filename="mainwindow.cpp" line="1381"/>
        <location filename="ui_mainwindow.h" line="486"/>
        <source>Install</source>
        <translation>Asenna</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="489"/>
        <source>Quit application</source>
        <translation>Sulje sovellus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="491"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="682"/>
        <location filename="ui_mainwindow.h" line="492"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Uninstalling packages...</source>
        <translation>Poistetaan paketteja...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Running apt-get update... </source>
        <translation>Suoritetaan apt-get update...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <location filename="mainwindow.cpp" line="1187"/>
        <location filename="mainwindow.cpp" line="1284"/>
        <source>Cancel</source>
        <translation>Peruuta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="304"/>
        <source>Please wait...</source>
        <translation>Odota, ole hyvä...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>Updating package list...</source>
        <translation>Päivittää pakettilistausta...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Version </source>
        <translation>Versio</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source> in stable repo</source>
        <translation>vakaassa pakettivarastossa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Not available in stable repo</source>
        <translation>Ei saatavana vakaassa pakettivarastossa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source>Latest version </source>
        <translation>Viimeisin versio</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source> already installed</source>
        <translation>On jo asennettu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source> installed</source>
        <translation>asennettu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="526"/>
        <source>Warning</source>
        <translation>Varoitus</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and antiX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation>Olet aikomuksessa käyttää Debian:in Takaporttiohjelmistolähdettä, joka sisältää paketteja, jotka ovat otetut tulevasta Debian:in julkaisusta (nimeltään &apos;testing&apos;), säädettynä ja uudelleenkoottuna käytettäväksi sitten Debian:in vakaassa julkaisussa myöhemmin. Niitä ei ole voitu testata yhtä läpikotaisesti kuten Debian:in ja antiX:in vakaiden julkaisujen suhteen ja ne on annettu tarjolle sellaisena kuin ne sillä hetkellä ovat, sisältäen yhteensopivuusriskejä Debian:in vakaaseenjulkaisuun verrattuna. Käytä siis varauksella! </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="535"/>
        <source>Do not show this message again</source>
        <translation>Älä näytä tätä viestiä uudelleen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>Internet ei ole käytettävissä, pakettilistauksen lataaminen ei onnistu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="580"/>
        <source>Installing packages...</source>
        <translation>Asennetaan paketteja...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Installing...</source>
        <translation>Asennetaan...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Post-processing...</source>
        <translation>Jälkikäsittely käynnissä...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="637"/>
        <source>Pre-processing for </source>
        <translation>Esikäsittely kohteelle</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="643"/>
        <source>Installing </source>
        <translation>Asennetaan</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="648"/>
        <source>Post-processing for </source>
        <translation>Jälkikäsittely kohteelle</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="697"/>
        <source>Done</source>
        <translation>Valmis</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <source>Process finished.&lt;p&gt;&lt;b&gt;Do you want to exit Package Installer?&lt;/b&gt;</source>
        <translation>Käsittely valmistunut.&lt;p&gt;&lt;b&gt;Haluatko poistua Paketinasentajasta?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>Yes</source>
        <translation>Kyllä</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>No</source>
        <translation>Ei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="775"/>
        <source>Downloading package info...</source>
        <translation>Paketin tietoja ladataan...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="853"/>
        <source>Reading downloaded file...</source>
        <translation>Luetaan ladattua tiedostoa...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1053"/>
        <source>Packages to be installed: </source>
        <translation>Paketit, jotka tullaan asentamaan:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Package info</source>
        <translation>Paketin tiedot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1181"/>
        <source>About Package Installer</source>
        <translation>Paketinasentajaan liittyvää tietoa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1182"/>
        <source>Version: </source>
        <translation>Versio: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1183"/>
        <source>Package Installer for antiX Linux</source>
        <translation>Pakettien asennusohjelma antiX Linuxille</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1185"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1186"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <source>License</source>
        <translation>Lisenssi</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1228"/>
        <source>Reinstall</source>
        <translation>Asenna uudelleen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1279"/>
        <source>Repo Selection</source>
        <translation>Pakettivaraston valinta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1280"/>
        <source>Please select repo to load</source>
        <translation>Ole hyvä ja valitse ladattava pakettivarasto</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1379"/>
        <source>Upgrade</source>
        <translation>Päivitä</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="56"/>
        <source>Unable to get exclusive lock</source>
        <translation>Ensisijaista lukitusta ei saatu</translation>
    </message>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Jokin toinen paketinhallintaohjelma (kuten Synaptic tai apt-get), on jo valmiiksi käynnissä. Ole hyvä ja sulje tuo ohjelma ensin</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>You must run this program as root.</source>
        <translation>Sinun täytyy suorittaa tämä ohjelma pääkäyttäjänä.</translation>
    </message>
</context>
</TS>