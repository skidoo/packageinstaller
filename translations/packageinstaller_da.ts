<?xml version="1.0" ?><!DOCTYPE TS><TS language="da" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="mainwindow.cpp" line="1182"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <location filename="mainwindow.cpp" line="1197"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Package Installer</source>
        <translation>Pakkeinstallationsprogram</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="42"/>
        <location filename="ui_mainwindow.h" line="440"/>
        <source>Popular Applications</source>
        <translation>Populære programmer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Package</source>
        <translation>Pakke</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="436"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <location filename="mainwindow.ui" line="363"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="435"/>
        <location filename="ui_mainwindow.h" line="462"/>
        <source>Description</source>
        <translation>Beskrivelse</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <location filename="mainwindow.ui" line="332"/>
        <location filename="ui_mainwindow.h" line="438"/>
        <location filename="ui_mainwindow.h" line="459"/>
        <source>search</source>
        <translation>søg</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Greyed out items have already been installed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Håndter populære pakker&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Grå punkter er allerede installeret.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <location filename="ui_mainwindow.h" line="474"/>
        <source>Full App Catalog</source>
        <translation>Fuldt programkatalog</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="144"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Select source</source>
        <translation>Vælg kilde</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <location filename="mainwindow.cpp" line="1283"/>
        <location filename="ui_mainwindow.h" line="442"/>
        <source>Stable Repo</source>
        <translation>Stable-softwarekilde</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="ui_mainwindow.h" line="443"/>
        <source>MX Test Repo</source>
        <translation>MX test-softwarekilde</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="170"/>
        <location filename="mainwindow.cpp" line="1281"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Debian Backports Repo</source>
        <translation>Debian backports-softwarekilde</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="454"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filtrer pakker efter deres status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="mainwindow.ui" line="265"/>
        <location filename="mainwindow.cpp" line="1323"/>
        <location filename="ui_mainwindow.h" line="448"/>
        <location filename="ui_mainwindow.h" line="456"/>
        <source>All packages</source>
        <translation>Alle pakker</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <location filename="mainwindow.cpp" line="1336"/>
        <location filename="ui_mainwindow.h" line="449"/>
        <source>Installed</source>
        <translation>Installeret</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="275"/>
        <location filename="mainwindow.cpp" line="1334"/>
        <location filename="ui_mainwindow.h" line="450"/>
        <source>Upgradable</source>
        <translation>Kan opgraderes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="ui_mainwindow.h" line="451"/>
        <source>Not installed</source>
        <translation>Ikke installeret</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="457"/>
        <source>Greyed out items have already been installed.</source>
        <translation>Grå punkter er allerede installeret.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="458"/>
        <source>Refresh list</source>
        <translation>Genopfrisk liste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="464"/>
        <source>Package Name</source>
        <translation>Pakkenavn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="463"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="461"/>
        <source>Displayed</source>
        <translation>Vist</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <location filename="ui_mainwindow.h" line="465"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= Upgradable package. Newer version available in selected repository.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= Pakke som kan opgraderes. Nyere version tilgængelig i valgte arkiv.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="466"/>
        <source>Total packages:</source>
        <translation>Samlet pakker:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <location filename="ui_mainwindow.h" line="468"/>
        <source>Installed:</source>
        <translation>Installeret:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="463"/>
        <location filename="ui_mainwindow.h" line="470"/>
        <source>Upgradable:</source>
        <translation>Kan opgraderes:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="477"/>
        <location filename="ui_mainwindow.h" line="472"/>
        <source>Upgrade All</source>
        <translation>Opgrader alle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="473"/>
        <source>Hide library and developer packages</source>
        <translation>Skjul biblioteks- og udviklerpakker</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="545"/>
        <location filename="ui_mainwindow.h" line="475"/>
        <source>Uninstall</source>
        <translation>Afinstaller</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="579"/>
        <location filename="ui_mainwindow.h" line="477"/>
        <source>About this application</source>
        <translation>Om programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="582"/>
        <location filename="ui_mainwindow.h" line="479"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <location filename="ui_mainwindow.h" line="480"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="606"/>
        <location filename="ui_mainwindow.h" line="482"/>
        <source>Display help </source>
        <translation>Vis hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="ui_mainwindow.h" line="484"/>
        <source>Help</source>
        <translation>Hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="615"/>
        <location filename="ui_mainwindow.h" line="485"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="628"/>
        <location filename="mainwindow.cpp" line="1230"/>
        <location filename="mainwindow.cpp" line="1381"/>
        <location filename="ui_mainwindow.h" line="486"/>
        <source>Install</source>
        <translation>Installer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="489"/>
        <source>Quit application</source>
        <translation>Afslut programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="491"/>
        <source>Close</source>
        <translation>Luk</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="682"/>
        <location filename="ui_mainwindow.h" line="492"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Uninstalling packages...</source>
        <translation>Afinstallerer pakker...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Running apt-get update... </source>
        <translation>Kører apt-get update... </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <location filename="mainwindow.cpp" line="1187"/>
        <location filename="mainwindow.cpp" line="1284"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="304"/>
        <source>Please wait...</source>
        <translation>Vent venligst...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>Updating package list...</source>
        <translation>Opdaterer pakkeliste...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source> in stable repo</source>
        <translation> i stable-softwarekilde</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Not available in stable repo</source>
        <translation>Ikke tilgængelig i stable-softwarekilde</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source>Latest version </source>
        <translation>Seneste version</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source> already installed</source>
        <translation>allerede installeret</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source> installed</source>
        <translation>installeret</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="526"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and antiX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation>Du er ved at bruge Debian Backports, som indeholder pakker fra den næste Debian-udgivelse (kaldet &apos;testing&apos;), justeret og genkompileret til anvendelse på Debian stable. De kan ikke testes så vidtgående som stable-udgivelserne af Debian og antiX Linux, og leveres på et som-er grundlag, med risiko for inkompatibiliteter med andre komponenter i Debian stable. Brug med forsigtighed!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="535"/>
        <source>Do not show this message again</source>
        <translation>Vis ikke meddelelsen igen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>Internettet er ikke tilgængeligt, vil ikke være i stand til at downloade listen over pakkerne</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="580"/>
        <source>Installing packages...</source>
        <translation>Installerer pakker...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Installing...</source>
        <translation>Installerer...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Post-processing...</source>
        <translation>Efterbehandling...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="637"/>
        <source>Pre-processing for </source>
        <translation>Præbehandling til </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="643"/>
        <source>Installing </source>
        <translation>Installerer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="648"/>
        <source>Post-processing for </source>
        <translation>Efterbehandling til </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="697"/>
        <source>Done</source>
        <translation>Færdig</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <source>Process finished.&lt;p&gt;&lt;b&gt;Do you want to exit Package Installer?&lt;/b&gt;</source>
        <translation>Proces færdig.&lt;p&gt;&lt;b&gt;Vil du afslutte pakkeinstallationsprogrammet?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="775"/>
        <source>Downloading package info...</source>
        <translation>Downloader pakkeinfo...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="853"/>
        <source>Reading downloaded file...</source>
        <translation>Læser downloadet fil...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1053"/>
        <source>Packages to be installed: </source>
        <translation>Pakker som skal installeres: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Package info</source>
        <translation>Pakkeinfo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1181"/>
        <source>About Package Installer</source>
        <translation>Om pakkeinstallationsprogram</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1182"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1183"/>
        <source>Package Installer for antiX Linux</source>
        <translation>Pakkeinstallationsprogram til antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1185"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Ophavsret (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1186"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1228"/>
        <source>Reinstall</source>
        <translation>Geninstaller</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1279"/>
        <source>Repo Selection</source>
        <translation>Valg af softwarekilde</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1280"/>
        <source>Please select repo to load</source>
        <translation>Vælg venligt hvilken softwarekilde der skal indlæses</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1379"/>
        <source>Upgrade</source>
        <translation>Opgrader</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="56"/>
        <source>Unable to get exclusive lock</source>
        <translation>Kan ikke få eksklusiv lås</translation>
    </message>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Der kører allerede et andet pakkehåndteringsprogram (såsom Synaptic eller apt-get). Luk venligst programmet først</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>You must run this program as root.</source>
        <translation>Du skal køre programmet som root.</translation>
    </message>
</context>
</TS>