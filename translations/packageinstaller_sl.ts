<?xml version="1.0" ?><!DOCTYPE TS><TS language="sl" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="mainwindow.cpp" line="1182"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <location filename="mainwindow.cpp" line="1197"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Package Installer</source>
        <translation>Namestitev paketov</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="42"/>
        <location filename="ui_mainwindow.h" line="440"/>
        <source>Popular Applications</source>
        <translation>Priljubljeni programi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Package</source>
        <translation>Paket</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="436"/>
        <source>Info</source>
        <translation>Informacije</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <location filename="mainwindow.ui" line="363"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="435"/>
        <location filename="ui_mainwindow.h" line="462"/>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <location filename="mainwindow.ui" line="332"/>
        <location filename="ui_mainwindow.h" line="438"/>
        <location filename="ui_mainwindow.h" line="459"/>
        <source>search</source>
        <translation>iskanje</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Greyed out items have already been installed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Upravljanje priljubljenih paketov&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Sivo obarvani paketi so že bili nameščeni.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <location filename="ui_mainwindow.h" line="474"/>
        <source>Full App Catalog</source>
        <translation>Celotni katalog programov</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="144"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Select source</source>
        <translation>Izberi vir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <location filename="mainwindow.cpp" line="1283"/>
        <location filename="ui_mainwindow.h" line="442"/>
        <source>Stable Repo</source>
        <translation>Stabilni repo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="ui_mainwindow.h" line="443"/>
        <source>MX Test Repo</source>
        <translation>MX Testiranje repozitorija</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="170"/>
        <location filename="mainwindow.cpp" line="1281"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Debian Backports Repo</source>
        <translation>Debian backport repozitorij</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="454"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filtriranje paketov glede na njihovo status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="mainwindow.ui" line="265"/>
        <location filename="mainwindow.cpp" line="1323"/>
        <location filename="ui_mainwindow.h" line="448"/>
        <location filename="ui_mainwindow.h" line="456"/>
        <source>All packages</source>
        <translation>Vsi paketi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <location filename="mainwindow.cpp" line="1336"/>
        <location filename="ui_mainwindow.h" line="449"/>
        <source>Installed</source>
        <translation>Nameščeno</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="275"/>
        <location filename="mainwindow.cpp" line="1334"/>
        <location filename="ui_mainwindow.h" line="450"/>
        <source>Upgradable</source>
        <translation>Nadgradljivo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="ui_mainwindow.h" line="451"/>
        <source>Not installed</source>
        <translation>Ni nameščeno</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="457"/>
        <source>Greyed out items have already been installed.</source>
        <translation>Osiveli paketi so že bili nameščeni.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="458"/>
        <source>Refresh list</source>
        <translation>Osveži seznam</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="464"/>
        <source>Package Name</source>
        <translation>Ime paketa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="463"/>
        <source>Version</source>
        <translation>Različica</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="461"/>
        <source>Displayed</source>
        <translation>Prikazani</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <location filename="ui_mainwindow.h" line="465"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= Upgradable package. Newer version available in selected repository.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= nadgradljiv paket. V izbranem skladišču je na voljo novejša različica.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="466"/>
        <source>Total packages:</source>
        <translation>Skupaj paketov:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <location filename="ui_mainwindow.h" line="468"/>
        <source>Installed:</source>
        <translation>Nameščenih:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="463"/>
        <location filename="ui_mainwindow.h" line="470"/>
        <source>Upgradable:</source>
        <translation>Nadgradljivih:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="477"/>
        <location filename="ui_mainwindow.h" line="472"/>
        <source>Upgrade All</source>
        <translation>Nadgradi vse</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="473"/>
        <source>Hide library and developer packages</source>
        <translation>Skrij knjižnice in razvojne pakete</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="545"/>
        <location filename="ui_mainwindow.h" line="475"/>
        <source>Uninstall</source>
        <translation>Odstrani</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="579"/>
        <location filename="ui_mainwindow.h" line="477"/>
        <source>About this application</source>
        <translation>O tem programu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="582"/>
        <location filename="ui_mainwindow.h" line="479"/>
        <source>About...</source>
        <translation>O programu...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <location filename="ui_mainwindow.h" line="480"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="606"/>
        <location filename="ui_mainwindow.h" line="482"/>
        <source>Display help </source>
        <translation>Prikaži pomoč</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="ui_mainwindow.h" line="484"/>
        <source>Help</source>
        <translation>Pomoč</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="615"/>
        <location filename="ui_mainwindow.h" line="485"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="628"/>
        <location filename="mainwindow.cpp" line="1230"/>
        <location filename="mainwindow.cpp" line="1381"/>
        <location filename="ui_mainwindow.h" line="486"/>
        <source>Install</source>
        <translation>Namesti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="489"/>
        <source>Quit application</source>
        <translation>Zapri aplikacijo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="491"/>
        <source>Close</source>
        <translation>Zapri</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="682"/>
        <location filename="ui_mainwindow.h" line="492"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Uninstalling packages...</source>
        <translation>Odstranjevanje paketov</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Running apt-get update... </source>
        <translation>Izvajam apt-get update...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <location filename="mainwindow.cpp" line="1187"/>
        <location filename="mainwindow.cpp" line="1284"/>
        <source>Cancel</source>
        <translation>Prekini</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="304"/>
        <source>Please wait...</source>
        <translation>Prosimo, počakajt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>Updating package list...</source>
        <translation>Osvežujem seznam paketov...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Version </source>
        <translation>Različica</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source> in stable repo</source>
        <translation>v stabilnem repoju</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Not available in stable repo</source>
        <translation>Ni na voljo v stabilnem repozitoriju</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source>Latest version </source>
        <translation>Zadnja različica</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source> already installed</source>
        <translation>je že nameščena</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source> installed</source>
        <translation>nameščena</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="526"/>
        <source>Warning</source>
        <translation>Opozorilo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and antiX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation>Ste na tem, da boste uporabili Debian backporte, ki vsebujejo pakete iz naslednjih izdaj Debiana (poimenovano &apos;testing&apos;) in ki so prilagojeni za rabo s stabilnim Debianom. Niso preizkušeni tako intenzivno, kot stabilne izdaje za Debian ali MX Linux in so lahko nezdružljivi z drugimi komponentami stabilnega Debiana. Uporabite previdno!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="535"/>
        <source>Do not show this message again</source>
        <translation>Ne prikaži več tega sporočila.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Error</source>
        <translation>Napaka</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>Ni povezave s spletom, tao ni mogo9če naložiti seznama paketov</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="580"/>
        <source>Installing packages...</source>
        <translation>Nameščanje paketov...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Installing...</source>
        <translation>Nameščam...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Post-processing...</source>
        <translation>Post-procesiranje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="637"/>
        <source>Pre-processing for </source>
        <translation>Pred-procesiranje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="643"/>
        <source>Installing </source>
        <translation>Nameščanje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="648"/>
        <source>Post-processing for </source>
        <translation>Po-procesiranje za</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="697"/>
        <source>Done</source>
        <translation>Zaključeno</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <source>Process finished.&lt;p&gt;&lt;b&gt;Do you want to exit Package Installer?&lt;/b&gt;</source>
        <translation>Postopek je zaključen.&lt;p&gt;&lt;b&gt;Ali bi radi zaprli program za nameščanje paketov?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>Yes</source>
        <translation>Da</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="775"/>
        <source>Downloading package info...</source>
        <translation>Prenašanje informacij o paketu...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="853"/>
        <source>Reading downloaded file...</source>
        <translation>Berem preneseno datoteko...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1053"/>
        <source>Packages to be installed: </source>
        <translation>Paketi, ki bodo nameščeni:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Package info</source>
        <translation>Informacije o paketu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1181"/>
        <source>About Package Installer</source>
        <translation>O programu Namestitev paketov</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1182"/>
        <source>Version: </source>
        <translation>Različica:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1183"/>
        <source>Package Installer for antiX Linux</source>
        <translation>Namestilnik paketov za antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1185"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1186"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <source>License</source>
        <translation>Licenca</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1228"/>
        <source>Reinstall</source>
        <translation>Ponovno namesti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1279"/>
        <source>Repo Selection</source>
        <translation>Izbor repozitorija</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1280"/>
        <source>Please select repo to load</source>
        <translation>Prosimo, izberite repozitorij, ki naj se naloži</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1379"/>
        <source>Upgrade</source>
        <translation>Nadgradi</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="56"/>
        <source>Unable to get exclusive lock</source>
        <translation>Ekskluzivni zaklep ni mogoč</translation>
    </message>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Zagnana je še ena aplikacija za nameščanje programov (kot sta Synaptic ali apt-get). Zaprite najprej to aplikacijo</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>You must run this program as root.</source>
        <translation>Ta program morate zagnati korensko</translation>
    </message>
</context>
</TS>