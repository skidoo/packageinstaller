<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="mainwindow.cpp" line="1182"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <location filename="mainwindow.cpp" line="1197"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Package Installer</source>
        <translation>Paket Kurucu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="42"/>
        <location filename="ui_mainwindow.h" line="440"/>
        <source>Popular Applications</source>
        <translation>Gözde Uygulamalar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Package</source>
        <translation>Paket</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="436"/>
        <source>Info</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <location filename="mainwindow.ui" line="363"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="435"/>
        <location filename="ui_mainwindow.h" line="462"/>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <location filename="mainwindow.ui" line="332"/>
        <location filename="ui_mainwindow.h" line="438"/>
        <location filename="ui_mainwindow.h" line="459"/>
        <source>search</source>
        <translation>Ara</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Greyed out items have already been installed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Gözde Uygulamalar&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Gri renkte yazılı öğeler zaten kurulu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <location filename="ui_mainwindow.h" line="474"/>
        <source>Full App Catalog</source>
        <translation>Tam Uygulama Kataloğu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="144"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Select source</source>
        <translation>Kaynak seç</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <location filename="mainwindow.cpp" line="1283"/>
        <location filename="ui_mainwindow.h" line="442"/>
        <source>Stable Repo</source>
        <translation>Kararlı depo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="ui_mainwindow.h" line="443"/>
        <source>MX Test Repo</source>
        <translation>MX Test Deposu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="170"/>
        <location filename="mainwindow.cpp" line="1281"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Debian Backports Repo</source>
        <translation>Debian Backports Deposu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="454"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Paketleri durumlarına göre süzün.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="mainwindow.ui" line="265"/>
        <location filename="mainwindow.cpp" line="1323"/>
        <location filename="ui_mainwindow.h" line="448"/>
        <location filename="ui_mainwindow.h" line="456"/>
        <source>All packages</source>
        <translation>Tüm paketler</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <location filename="mainwindow.cpp" line="1336"/>
        <location filename="ui_mainwindow.h" line="449"/>
        <source>Installed</source>
        <translation>Kurulu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="275"/>
        <location filename="mainwindow.cpp" line="1334"/>
        <location filename="ui_mainwindow.h" line="450"/>
        <source>Upgradable</source>
        <translation>Yükseltilebilir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="ui_mainwindow.h" line="451"/>
        <source>Not installed</source>
        <translation>Kurulu değil</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="457"/>
        <source>Greyed out items have already been installed.</source>
        <translation>Gri renkte yazılı öğeler zaten kurulu.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="458"/>
        <source>Refresh list</source>
        <translation>Listeyi tazele</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="464"/>
        <source>Package Name</source>
        <translation>Paket Adı</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="463"/>
        <source>Version</source>
        <translation>Sürüm</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="461"/>
        <source>Displayed</source>
        <translation>Gösterilen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <location filename="ui_mainwindow.h" line="465"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= Upgradable package. Newer version available in selected repository.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;=Yükseltilebilir paket. Seçili depoda daha yeni sürüm var.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="466"/>
        <source>Total packages:</source>
        <translation>Toplam paket:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <location filename="ui_mainwindow.h" line="468"/>
        <source>Installed:</source>
        <translation>Kurulu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="463"/>
        <location filename="ui_mainwindow.h" line="470"/>
        <source>Upgradable:</source>
        <translation>Yükseltilebilir:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="477"/>
        <location filename="ui_mainwindow.h" line="472"/>
        <source>Upgrade All</source>
        <translation>Hepsini Yükselt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="473"/>
        <source>Hide library and developer packages</source>
        <translation>Kütüphane ve geliştirici paketlerini gizle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="545"/>
        <location filename="ui_mainwindow.h" line="475"/>
        <source>Uninstall</source>
        <translation>Kaldır</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="579"/>
        <location filename="ui_mainwindow.h" line="477"/>
        <source>About this application</source>
        <translation>Uygulama hakkında</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="582"/>
        <location filename="ui_mainwindow.h" line="479"/>
        <source>About...</source>
        <translation>Hakkında...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <location filename="ui_mainwindow.h" line="480"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="606"/>
        <location filename="ui_mainwindow.h" line="482"/>
        <source>Display help </source>
        <translation>Yardımı görüntüle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="ui_mainwindow.h" line="484"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="615"/>
        <location filename="ui_mainwindow.h" line="485"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="628"/>
        <location filename="mainwindow.cpp" line="1230"/>
        <location filename="mainwindow.cpp" line="1381"/>
        <location filename="ui_mainwindow.h" line="486"/>
        <source>Install</source>
        <translation>Yükle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="489"/>
        <source>Quit application</source>
        <translation>Uygulamadan çık</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="491"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="682"/>
        <location filename="ui_mainwindow.h" line="492"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Uninstalling packages...</source>
        <translation>Paketleri kaldırma...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Running apt-get update... </source>
        <translation>apt-get update çalışıyor...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <location filename="mainwindow.cpp" line="1187"/>
        <location filename="mainwindow.cpp" line="1284"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="304"/>
        <source>Please wait...</source>
        <translation>Lütfen bekleyin...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>Updating package list...</source>
        <translation>Paket listesi güncelleniyor...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Version </source>
        <translation>Sürüm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source> in stable repo</source>
        <translation>kararlı depoda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Not available in stable repo</source>
        <translation>Kararlı depo mevcut değil</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source>Latest version </source>
        <translation>En son sürüm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source> already installed</source>
        <translation>zaten kurulu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source> installed</source>
        <translation>kurulu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="526"/>
        <source>Warning</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and antiX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation>Debian Kararlı Sürümü için ayarlanıp yeniden derlenen; bir sonraki Debian sürümünden alınan (&apos;test&apos; adı verilen) paketleri içeren Debian Backports kullanmak üzeresiniz. Debian ve antiX Linux’un kararlı sürümlerinde olduğu gibi kapsamlı bir şekilde test edilememekteler ve Debian Kararlı Sürümdeki diğer bileşenlerle uyumsuzluk riskiyle beraber &quot;oldukları gibi&quot; sağlanmaktalar. Dikkatli kullanın!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="535"/>
        <source>Do not show this message again</source>
        <translation>Bu iletiyi tekrar gösterme</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>İnternete bağlı değilsiniz, paket listesi indirilemeyecek.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="580"/>
        <source>Installing packages...</source>
        <translation>Paketler kuruluyor...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Installing...</source>
        <translation>Kuruluyor...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Post-processing...</source>
        <translation>Son işlem...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="637"/>
        <source>Pre-processing for </source>
        <translation>Ön işlemi</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="643"/>
        <source>Installing </source>
        <translation>Kuruluyor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="648"/>
        <source>Post-processing for </source>
        <translation>Son işlemi</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="697"/>
        <source>Done</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <source>Process finished.&lt;p&gt;&lt;b&gt;Do you want to exit Package Installer?&lt;/b&gt;</source>
        <translation>İşlem bitti.&lt;p&gt;&lt;b&gt;Paket Yükleyiciden çıkmak istiyor musunuz?&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>No</source>
        <translation>Hayır</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="775"/>
        <source>Downloading package info...</source>
        <translation>Paket bilgisi indiriliyor...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="853"/>
        <source>Reading downloaded file...</source>
        <translation>İndirilen dosyalar okunuyor...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1053"/>
        <source>Packages to be installed: </source>
        <translation>Yüklenecek paketler:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Package info</source>
        <translation>Paket bilgisi</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1181"/>
        <source>About Package Installer</source>
        <translation>Paket Yükleyici Hakkında</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1182"/>
        <source>Version: </source>
        <translation>Sürüm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1183"/>
        <source>Package Installer for antiX Linux</source>
        <translation>antiX Linux için Paket Yükleyici</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1185"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1186"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <source>License</source>
        <translation>Ruhsat</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1228"/>
        <source>Reinstall</source>
        <translation>Yeniden kur</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1279"/>
        <source>Repo Selection</source>
        <translation>Depo Seçimi</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1280"/>
        <source>Please select repo to load</source>
        <translation>Yüklemek için depo seçiniz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1379"/>
        <source>Upgrade</source>
        <translation>Yükseltme</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="56"/>
        <source>Unable to get exclusive lock</source>
        <translation>Özel kilit alınamadı</translation>
    </message>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Başka bir paket yönetim uygulaması (Synaptic veya apt-get gibi) zaten çalışıyor. Lütfen önce bu uygulamayı kapatın</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>You must run this program as root.</source>
        <translation>Bu programı root olarak çalıştırmalısınız.</translation>
    </message>
</context>
</TS>