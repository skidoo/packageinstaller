<?xml version="1.0" ?><!DOCTYPE TS><TS language="lt" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="mainwindow.cpp" line="75"/>
        <location filename="mainwindow.cpp" line="1182"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <location filename="mainwindow.cpp" line="1197"/>
        <location filename="ui_mainwindow.h" line="433"/>
        <source>Package Installer</source>
        <translation>Paketų diegimo programa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="42"/>
        <location filename="ui_mainwindow.h" line="440"/>
        <source>Popular Applications</source>
        <translation>Populiarios programos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="437"/>
        <source>Package</source>
        <translation>Paketas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="436"/>
        <source>Info</source>
        <translation>Informacija</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <location filename="mainwindow.ui" line="363"/>
        <location filename="mainwindow.cpp" line="78"/>
        <location filename="ui_mainwindow.h" line="435"/>
        <location filename="ui_mainwindow.h" line="462"/>
        <source>Description</source>
        <translation>Aprašas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <location filename="mainwindow.ui" line="332"/>
        <location filename="ui_mainwindow.h" line="438"/>
        <location filename="ui_mainwindow.h" line="459"/>
        <source>search</source>
        <translation>ieškoti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <location filename="ui_mainwindow.h" line="439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Manage popular packages&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Greyed out items have already been installed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt;&quot;&gt;Tvarkyti populiarius paketus&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Pilka spalva atvaizduojami elementai jau yra įdiegti.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="138"/>
        <location filename="ui_mainwindow.h" line="474"/>
        <source>Full App Catalog</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="144"/>
        <location filename="ui_mainwindow.h" line="441"/>
        <source>Select source</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <location filename="mainwindow.cpp" line="1283"/>
        <location filename="ui_mainwindow.h" line="442"/>
        <source>Stable Repo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="160"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="ui_mainwindow.h" line="443"/>
        <source>MX Test Repo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="170"/>
        <location filename="mainwindow.cpp" line="1281"/>
        <location filename="ui_mainwindow.h" line="444"/>
        <source>Debian Backports Repo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <location filename="ui_mainwindow.h" line="454"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filter packages according to their status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Filtruoti paketus atitinkamai pagal jų būseną.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <location filename="mainwindow.ui" line="265"/>
        <location filename="mainwindow.cpp" line="1323"/>
        <location filename="ui_mainwindow.h" line="448"/>
        <location filename="ui_mainwindow.h" line="456"/>
        <source>All packages</source>
        <translation>Visi paketai</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <location filename="mainwindow.cpp" line="1336"/>
        <location filename="ui_mainwindow.h" line="449"/>
        <source>Installed</source>
        <translation>Įdiegti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="275"/>
        <location filename="mainwindow.cpp" line="1334"/>
        <location filename="ui_mainwindow.h" line="450"/>
        <source>Upgradable</source>
        <translation>Naujintini</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="ui_mainwindow.h" line="451"/>
        <source>Not installed</source>
        <translation>Neįdiegti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="457"/>
        <source>Greyed out items have already been installed.</source>
        <translation>Pilka spalva pažymėti elementai jau yra įdiegti.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="313"/>
        <location filename="ui_mainwindow.h" line="458"/>
        <source>Refresh list</source>
        <translation>Įkelti sąrašą iš naujo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="464"/>
        <source>Package Name</source>
        <translation>Paketo pavadinimas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="ui_mainwindow.h" line="463"/>
        <source>Version</source>
        <translation>Versija</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <location filename="ui_mainwindow.h" line="461"/>
        <source>Displayed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="381"/>
        <location filename="ui_mainwindow.h" line="465"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= Upgradable package. Newer version available in selected repository.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;= Naujintinas paketas. Pasirinktoje saugykloje yra prieinama naujesnė versija.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="403"/>
        <location filename="ui_mainwindow.h" line="466"/>
        <source>Total packages:</source>
        <translation>Viso paketų:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <location filename="ui_mainwindow.h" line="468"/>
        <source>Installed:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="463"/>
        <location filename="ui_mainwindow.h" line="470"/>
        <source>Upgradable:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="477"/>
        <location filename="ui_mainwindow.h" line="472"/>
        <source>Upgrade All</source>
        <translation>Naujinti visus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="473"/>
        <source>Hide library and developer packages</source>
        <translation>Slėpti bibliotekas ir plėtotojų paketus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="545"/>
        <location filename="ui_mainwindow.h" line="475"/>
        <source>Uninstall</source>
        <translation>Šalinti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="579"/>
        <location filename="ui_mainwindow.h" line="477"/>
        <source>About this application</source>
        <translation>Apie šią programą</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="582"/>
        <location filename="ui_mainwindow.h" line="479"/>
        <source>About...</source>
        <translation>Apie...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <location filename="ui_mainwindow.h" line="480"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="606"/>
        <location filename="ui_mainwindow.h" line="482"/>
        <source>Display help </source>
        <translation>Rodyti žinyną</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="ui_mainwindow.h" line="484"/>
        <source>Help</source>
        <translation>Žinynas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="615"/>
        <location filename="ui_mainwindow.h" line="485"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="628"/>
        <location filename="mainwindow.cpp" line="1230"/>
        <location filename="mainwindow.cpp" line="1381"/>
        <location filename="ui_mainwindow.h" line="486"/>
        <source>Install</source>
        <translation>Įdiegti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="489"/>
        <source>Quit application</source>
        <translation>Išeiti iš programos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="491"/>
        <source>Close</source>
        <translation>Užverti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="682"/>
        <location filename="ui_mainwindow.h" line="492"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Uninstalling packages...</source>
        <translation>Šalinami paketai...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Running apt-get update... </source>
        <translation>Vykdoma apt-get update... </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <location filename="mainwindow.cpp" line="1187"/>
        <location filename="mainwindow.cpp" line="1284"/>
        <source>Cancel</source>
        <translation>Atsisakyti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="304"/>
        <source>Please wait...</source>
        <translation>Prašome palaukti...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>Updating package list...</source>
        <translation>Atnaujinamas paketų sąrašas...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Version </source>
        <translation>Versija </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source> in stable repo</source>
        <translation> stabilioje saugykloje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <source>Not available in stable repo</source>
        <translation>Neprieinama stabilioje saugykloje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source>Latest version </source>
        <translation>Naujausia versija </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="488"/>
        <source> already installed</source>
        <translation> jau įdiegta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source> installed</source>
        <translation> įdiegta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="526"/>
        <source>Warning</source>
        <translation>Įspėjimas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>You are about to use Debian Backports, which contains packages taken from the next Debian release (called &apos;testing&apos;), adjusted and recompiled for usage on Debian stable. They cannot be tested as extensively as in the stable releases of Debian and antiX Linux, and are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="535"/>
        <source>Do not show this message again</source>
        <translation>Daugiau neberodyti šio pranešimo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Error</source>
        <translation>Klaida</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="662"/>
        <location filename="mainwindow.cpp" line="767"/>
        <source>Internet is not available, won&apos;t be able to download the list of packages</source>
        <translation>Internetas neprieinamas, nepavyks atsisiųsti paketų sąrašo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="580"/>
        <source>Installing packages...</source>
        <translation>Įdiegiami paketai...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="609"/>
        <source>Installing...</source>
        <translation>Įdiegiama...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Post-processing...</source>
        <translation>Po-apdorojimas...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="637"/>
        <source>Pre-processing for </source>
        <translation>Prieš-apdorojimas, skirtas </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="643"/>
        <source>Installing </source>
        <translation>Įdiegiama </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="648"/>
        <source>Post-processing for </source>
        <translation>Po-apdorojimas, skirtas </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="697"/>
        <source>Done</source>
        <translation>Atlikta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="698"/>
        <source>Process finished.&lt;p&gt;&lt;b&gt;Do you want to exit Package Installer?&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>Yes</source>
        <translation>Taip</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="699"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="775"/>
        <source>Downloading package info...</source>
        <translation>Atsiunčiama paketo informacija...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="853"/>
        <source>Reading downloaded file...</source>
        <translation>Skaitomas atsisiųstas failas...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1053"/>
        <source>Packages to be installed: </source>
        <translation>Paketai, kurie bus įdiegti: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Package info</source>
        <translation>Paketo informacija</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1181"/>
        <source>About Package Installer</source>
        <translation>Apie paketų diegimo programą</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1182"/>
        <source>Version: </source>
        <translation>Versija: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1183"/>
        <source>Package Installer for antiX Linux</source>
        <translation>Paketų diegimo programa, skirta antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1185"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Autorių teisės (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1186"/>
        <location filename="mainwindow.cpp" line="1189"/>
        <source>License</source>
        <translation>Licencija</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1228"/>
        <source>Reinstall</source>
        <translation>Įdiegti iš naujo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1279"/>
        <source>Repo Selection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1280"/>
        <source>Please select repo to load</source>
        <translation>Pasirinkite saugyklą, kurią įkelti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1379"/>
        <source>Upgrade</source>
        <translation>Naujinti</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="main.cpp" line="56"/>
        <source>Unable to get exclusive lock</source>
        <translation>Nepavyko gauti išskirtinio užrakto</translation>
    </message>
    <message>
        <location filename="main.cpp" line="57"/>
        <source>Another package management application (like Synaptic or apt-get), is already running. Please close that application first</source>
        <translation>Jau yra vykdoma kita (tokia kaip Synaptic ar apt-get) paketų tvarkymo programa. Prašome, iš pradžių, užverti tą programą</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>You must run this program as root.</source>
        <translation>Privalote paleisti šią programą kaip pagrindinis (root) naudotojas.</translation>
    </message>
</context>
</TS>